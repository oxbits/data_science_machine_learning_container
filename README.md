```
git clone https://gitlab.com/oxbits/data_science_machine_learning_container.git

cd data_science_machine_learning_container

docker build -t dsml_proj .

docker run -it --name a_dsml_proj --rm --publish 8420:8420 --volume $(pwd)/notebooks:/notebooks -w /notebooks dsml_proj bash

pip install jupyter scikit-learn matplotlib

jupyter notebook --ip=0.0.0.0 --port=8420 --allow-root
```
open the URL from standard output in your browser :

`http://127.0.0.1:8420/?token=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`

open the notebook :

`solution.ipynb`

run the notebook

see the results in `data_science_machine_learning_container/notebooks/data/` :

```
• 2D_fig.png
• 3D_fig.png
• example_database.db
• linear_fig.png
• linear_model.pickle
• stock_data.json
• stock_data.pickle
```

delete the results

in notebook UI_UX click `File` > `Close and Halt`

close browser

```
# ctrl-c then y then enter to stop jupyter notebook

pip freeze > requirements.txt

exit

docker build -t dsml_proj .

docker run -it --name a_dsml_proj --rm --publish 8420:8420 --volume $(pwd)/notebooks:/notebooks -w /notebooks dsml_proj /product.sh
```

see the results again in `data_science_machine_learning_container/notebooks/data/`
