FROM python

COPY ./notebooks/product.sh /product.sh

RUN chmod +x /product.sh

COPY ./notebooks/requirements.txt /requirements.txt

RUN pip install -r requirements.txt
